# ruff: noqa: T201 (print)

import sys
from collections.abc import Iterator, Sequence
from difflib import unified_diff

import typer
from sqlalchemy.schema import CreateTable, MetaData

from .database import engine
from .models import metadata

app = typer.Typer()


@app.command()
def diff() -> None:
    database = get_lines(get_database())
    models = get_lines(get_models())
    sys.stdout.writelines(unified_diff(database, models, 'database', 'models', n=16))


@app.command()
def models() -> None:
    for table in get_models():
        print(table)


@app.command()
def database() -> None:
    for table in get_database():
        print(table)


def get_models() -> Iterator[str]:
    return compile_metadata(metadata)


def get_database() -> Iterator[str]:
    metadata = MetaData()
    metadata.reflect(engine)

    for table in metadata.tables.values():
        for constraint in table.constraints:
            constraint.name = None

    return compile_metadata(metadata)


def compile_metadata(metadata: MetaData) -> Iterator[str]:
    return (str(CreateTable(table).compile(engine)) for table in metadata.sorted_tables)


def get_lines(tables: Iterator[str]) -> Sequence[str]:
    def get_lines(table: str) -> Sequence[str]:
        lines = table.splitlines(keepends=True)
        middle = lines[2:-2]

        # insert trailing comma
        last = middle[-1]
        last = last[0:-1] + ', ' + last[-1]

        middle[-1] = last
        middle.sort()
        return lines[0:2] + middle + lines[-2:]

    return tuple(line for table in tables for line in get_lines(table))


if __name__ == '__main__':
    app()
