# ruff: noqa: T201 (print)

import datetime
import imaplib
import io
import smtplib
from base64 import b64encode
from collections.abc import Iterable, Sequence
from decimal import Decimal
from email.message import EmailMessage
from email.policy import SMTP
from functools import cached_property
from itertools import chain, groupby
from typing import Annotated, Optional, TypeVar, cast
from urllib.parse import unquote

import typer
from airium import Airium  # type: ignore[import-untyped]
from dateutil.relativedelta import relativedelta
from pydantic import AnyUrl, BaseModel, SecretStr, UrlConstraints
from pydantic_settings import BaseSettings
from qrbill.bill import QRBill  # type: ignore[import-untyped]
from qrcode import QRCode  # type: ignore[import-untyped]
from reportlab.graphics import renderPDF  # type: ignore[import-untyped]
from sqlmodel import Session, col, func, or_, select
from stdnum.iso7064 import mod_97_10  # type: ignore[import-untyped]
from svglib.svglib import svg2rlg  # type: ignore[import-untyped]

from .database import begin
from .models import Bill, BillEntry, ComptaEcriture, Member, Membership, Payment


class EmailSettings(BaseSettings):
    smtp_uri: Annotated[AnyUrl, UrlConstraints(allowed_schemes=['smtps'], default_port=465)]
    smtp_password: SecretStr
    imap_uri: Annotated[AnyUrl, UrlConstraints(allowed_schemes=['imaps'], default_port=993)]
    imap_password: SecretStr


app = typer.Typer()


@app.command()
def monthly() -> None:
    now = datetime.datetime.now()
    cotis(now)
    email(now)


@app.command()
def cotis(
    now: datetime.datetime,
    *,
    member_id: Optional[int] = None,  # noqa: UP007 (int | None)
) -> None:
    today = now.date()
    cotis_from = datetime.date(today.year, today.month, 1)
    cotis_to = cotis_from + relativedelta(months=+1)
    ecriture_date = datetime.date(today.year, 12, 31)
    compte_debiteurs = 14
    compte_cotisations = 30

    with begin() as session:
        ecriture = session.exec(
            select(ComptaEcriture).where(
                ComptaEcriture.date == ecriture_date,
                ComptaEcriture.debit_id == compte_debiteurs,
                ComptaEcriture.credit_id == compte_cotisations,
            ),
        ).one_or_none()
        if ecriture is None:
            ecriture = ComptaEcriture(
                date=ecriture_date,
                debit_id=compte_debiteurs,
                credit_id=compte_cotisations,
                amount=0,
                label='Cotisations facturées',
            )
            session.add(ecriture)

        memberships = select(Membership)
        memberships = memberships.where(or_(col(Membership.entry).is_(None), col(Membership.entry) < cotis_to))
        memberships = memberships.where(or_(col(Membership.exit).is_(None), col(Membership.exit) > cotis_from))
        memberships = memberships.where(or_(member_id is None, Membership.member_id == member_id))
        memberships = memberships.order_by(
            col(Membership.entry).is_(None),
            col(Membership.entry).desc(),
            col(Membership.member_id),
        )
        for membership in session.exec(memberships):
            member = membership.member

            member_from = membership.entry
            member_from = cotis_from if member_from is None else max((member_from, cotis_from))
            member_to = membership.exit
            member_to = cotis_to if member_to is None else min((member_to, cotis_to))
            months_count = count_months(member_from, member_to)

            member_to -= datetime.timedelta(days=1)

            match membership.type:
                case Membership.Type.A:
                    item_price = 65
                    label = 'Cotisation membre actif'
                case Membership.Type.S:
                    item_price = 15
                    label = 'Cotisation membre de soutien'
                case _:
                    msg = f'unexpected membership type {membership.type}'
                    raise ValueError(msg)
            entry_price = item_price * months_count
            label = f'{label} {member_from} {member_to}'

            print(f'{member.id}\t{entry_price}\t{member.name}\t{label}')

            bill, position_max = session.exec(
                select(Bill, func.max(BillEntry.position))
                .join(BillEntry, isouter=True)
                .where(Bill.member == member, col(Bill.date) == today)
                .group_by(col(Bill.id))
                .order_by(col(Bill.id).desc()),
            ).first() or cast(tuple[Bill | None, int | None], (None, None))

            if bill is None:
                bill = Bill(
                    member=member,
                    date=today,
                )
                session.add(bill)
            if position_max is None:
                position_max = -1

            entry = BillEntry(
                bill=bill,
                position=position_max + 1,
                label=label,
                price=entry_price,
                ecriture=ecriture,
            )
            session.add(entry)


@app.command()
def email(
    now: datetime.datetime,
    *,
    member_id: Optional[int] = None,  # noqa: UP007 (int | None)
) -> None:
    today = now.date()

    settings = EmailSettings()

    smtp_uri = settings.smtp_uri
    smtp_host = not_none(smtp_uri.host)
    smtp_port = not_none(smtp_uri.port)
    smtp_user = not_none(smtp_uri.username)
    smtp_password = settings.smtp_password.get_secret_value()

    imap_uri = settings.imap_uri
    imap_host = not_none(imap_uri.host)
    imap_port = not_none(imap_uri.port)
    imap_user = not_none(imap_uri.username)
    imap_password = settings.imap_password.get_secret_value()
    imap_mailbox = unquote(not_none(imap_uri.path))[1:]

    with (
        begin() as session,
        smtplib.SMTP_SSL(smtp_host, smtp_port) as smtp,
        imaplib.IMAP4_SSL(imap_host, imap_port) as imap,
    ):
        smtp.login(smtp_user, smtp_password)
        imap.login(imap_user, imap_password)

        def bill_email(member: Member, bill: Bill | None, debiteur: Debiteur | None) -> None:
            print(f'{member.id} {member.name} {bill and bill.id} {debiteur and len(debiteur.rows)}')
            to = f'"{member.name}" <{member.email}>'
            message = email_message(to, bill_message(member, bill, debiteur))
            smtp.send_message(message)
            message_bytes = message.as_bytes(policy=SMTP)
            imap_time = imaplib.Time2Internaldate(datetime.datetime.now().astimezone())
            imap_assert(imap.append(f'"{imap_mailbox}"', '\\Seen', imap_time, message_bytes))

        debiteurs_iter = get_debiteurs(session, today, member_id=member_id)
        debiteurs = {debiteur.member.id: debiteur for debiteur in debiteurs_iter}
        for bill in session.exec(
            select(Bill).where(col(Bill.date) == today, or_(member_id is None, Bill.member_id == member_id)),
        ):
            member = bill.member
            debiteur = debiteurs.pop(member.id)
            bill_email(member, bill, debiteur)

        for debiteur in debiteurs.values():
            member = debiteur.member
            bill_email(member, None, debiteur)

        session.expunge_all()
        session.rollback()


T = TypeVar('T')


def not_none(value: T | None) -> T:
    if value is None:
        raise ValueError
    return value


def imap_assert(response: str | tuple[str, T]) -> T:
    status, data = cast(tuple[str, T], response)
    if status != 'OK':
        raise ValueError(response)
    return data


ZERO = Decimal('0.00')


class DebiteurRow(BaseModel):
    member_id: int
    date: datetime.date
    label: str
    amount: Decimal


class Debiteur(BaseModel):
    member: Member
    membership: Membership | None
    rows: Sequence[DebiteurRow]
    amount: Decimal


class Table(BaseModel):
    class Column(BaseModel):
        head: str
        body: Sequence[str]
        foot: str
        right_align: bool = False

        @cached_property
        def width(self) -> int:
            return max(len(cell) for cell in (self.head, *self.body, self.foot))

        def pad(self, cell: str) -> str:
            return cell.rjust(self.width) if self.right_align else cell.ljust(self.width)

        @cached_property
        def style(self) -> str:
            text_align = 'right' if self.right_align else 'left'
            return f'text-align: {text_align}'

    columns: Sequence[Column]

    @cached_property
    def rows(self) -> Sequence[Sequence[tuple[Column, str]]]:
        return tuple(zip(*(((column, cell) for cell in column.body) for column in self.columns), strict=False))

    def __init__(self, *columns: Column) -> None:
        super().__init__(columns=columns)

    def plain(self) -> str:
        plain_rows = (
            (column.pad(column.head) for column in self.columns),
            ('-' * column.width for column in self.columns),
            *((column.pad(cell) for column, cell in row) for row in self.rows),
            ('-' * column.width for column in self.columns),
            (column.pad(column.foot) for column in self.columns),
        )
        return '\n'.join('| ' + ' | '.join(row) + ' |' for row in plain_rows)

    def html(self, a: Airium) -> None:
        with a.table():
            with a.thead(style='border-bottom: solid;'), a.tr():
                for column in self.columns:
                    a.th(style=column.style, _t=column.head)
            with a.tbody():
                for row in self.rows:
                    with a.tr():
                        for column, cell in row:
                            a.td(style=column.style, _t=cell)
            with a.tfoot(style='border-top: solid;'), a.tr():
                for column in self.columns:
                    a.th(style=column.style, _t=column.foot)


class Message(BaseModel, arbitrary_types_allowed=True):
    title: str
    text: str
    bill_table: Table | None
    decompte_table: Table | None
    qr_bill: QRBill | None


def bill_message(member: Member, bill: Bill | None, debiteur: Debiteur | None) -> Message:
    if bill is not None:
        title = 'Facture de cotisation'
        text = """Voici ta facture de cotisation mensuelle.
Merci de l'acquitter d'ici la fin du mois."""
        amount = sum((entry.price for entry in bill.entries), ZERO)
        bill_table = Table(
            Table.Column(
                head='Libellé',
                body=tuple(entry.label for entry in bill.entries),
                foot='Total',
            ),
            Table.Column(
                head='Montant',
                body=tuple(f'{entry.price}' for entry in bill.entries),
                foot=f'{amount}',
                right_align=True,
            ),
        )
    else:
        title = 'Rappel de cotisation'
        text = """Nous avons constaté que tes cotisations sont à ce jour impayées.
Merci d'y remédier dès que possible."""
        bill_table = None

    if debiteur is not None:
        amount = -debiteur.amount
        debiteur_rows = debiteur.rows
        if bill is None or len(debiteur_rows) > 1:
            decompte_table = Table(
                Table.Column(
                    head='Date',
                    body=tuple(f'{row.date}' for row in debiteur_rows),
                    foot='',
                ),
                Table.Column(
                    head='Libellé',
                    body=tuple(row.label for row in debiteur_rows),
                    foot='A payer',
                ),
                Table.Column(
                    head='Montant',
                    body=tuple(f'{-row.amount}' for row in debiteur_rows),
                    foot=f'{amount}',
                    right_align=True,
                ),
            )
        else:
            decompte_table = None
    else:
        decompte_table = None

    if amount > ZERO:
        reference_number = creditor_reference(f'1{member.id}')
        qr_bill = QRBill(
            account='CH29 0900 0000 1024 9797 2',
            creditor=dict(
                name='Post Tenebras Lab',
                pcode='1227',
                city='Carouge',
                country='CH',
            ),
            amount=amount,
            debtor=dict(
                name=member.name,
                street=member.street,
                house_num=member.building,
                pcode=member.postcode,
                city=member.city,
                country=member.country,
            ),
            reference_number=reference_number,
            language='fr',
        )
    else:
        qr_bill = None

    return Message(
        title=title,
        text=text,
        bill_table=bill_table,
        decompte_table=decompte_table,
        qr_bill=qr_bill,
    )


def email_message(to: str, message: Message) -> EmailMessage:
    paragraphs = (
        'Chèr(e) membre,',
        message.text,
        "Avec nos meilleures salutations, ton comité qui t'aime.",
    )

    plain_paragraphs = list(paragraphs)
    if message.bill_table is not None:
        plain_paragraphs.append('# Facture')
        plain_paragraphs.append(message.bill_table.plain())
    if message.decompte_table is not None:
        plain_paragraphs.append('# Décompte')
        plain_paragraphs.append(message.decompte_table.plain())
    if message.qr_bill is not None:
        qr_str = qr_to_str(message.qr_bill)
        plain_paragraphs.append(qr_str)
    plain_str = '\n\n'.join(plain_paragraphs) + '\n'

    a = Airium()
    a('<!DOCTYPE html>')
    with a.html(lang='fr-CH'):
        with a.head():
            a.meta(charset='utf-8')
            a.meta(name='viewport', content='width=device-width, initial-scale=1')
            a.title(_t=message.title)
        with a.body():
            for paragraph in paragraphs:
                with a.p():
                    for line in paragraph.split('\n'):
                        a(line)
                        a.br()
            if message.bill_table is not None:
                a.h1(_t='Facture')
                message.bill_table.html(a)
            if message.decompte_table is not None:
                a.h1(_t='Décompte')
                with a.p():
                    message.decompte_table.html(a)
            if message.qr_bill is not None:
                with a.p():
                    qr_data_url = qr_to_data_url(message.qr_bill)
                    a.img(alt='QR-facture', src=qr_data_url)
    html_str = str(a)

    email = EmailMessage()
    email['From'] = '"PTL Comité" <comite@posttenebraslab.ch>'
    email['To'] = to
    email['Subject'] = f'[PTL] {message.title}'
    email.set_content(plain_str)
    email.add_alternative(html_str, subtype='html')
    if message.qr_bill is not None:
        qr_pdf = qr_to_pdf(message.qr_bill)
        email.add_attachment(qr_pdf, filename='QR-facture.pdf', maintype='application', subtype='pdf')
    return email


def get_debiteurs(
    session: Session,
    shrink_before: datetime.date,
    *,
    member_id: int | None = None,
) -> Iterable[Debiteur]:
    bills = (
        DebiteurRow(member_id=member_id, date=date, label='facturé', amount=-prices)
        for member_id, date, prices in session.exec(
            select(col(Bill.member_id), col(Bill.date), func.sum(col(BillEntry.price)))
            .join(BillEntry)
            .where(col(BillEntry.ecriture_id).isnot(None), or_(member_id is None, Bill.member_id == member_id))
            .group_by(col(Bill.id))
            .order_by(col(Bill.member_id), col(Bill.date), col(Bill.id)),
        )
    )
    payments = (
        DebiteurRow(member_id=member_id, date=date, label='encaissé', amount=amount)
        for member_id, date, amount in session.exec(
            select(col(Payment.member_id), col(Payment.date), col(Payment.value))
            .where(col(Payment.member_id).is_not(None), or_(member_id is None, Payment.member_id == member_id))
            .order_by(
                col(Payment.member_id),
                col(Payment.date),
                col(Payment.id),
            ),
        )
    )

    def shrink(rows: Iterable[DebiteurRow]) -> Sequence[DebiteurRow]:
        amount = ZERO
        result: list[DebiteurRow] = []
        for row in rows:
            amount += row.amount
            if amount.is_zero() and row.date < shrink_before:
                result.clear()
            else:
                result.append(row)
        return tuple(result)

    rows = {
        member_id: shrunk
        for member_id, rows in groupby(
            sorted(chain(bills, payments), key=lambda row: (row.member_id, row.date)),
            lambda row: row.member_id,
        )
        if (shrunk := shrink(rows))
    }

    member_ids = rows.keys()
    members = session.exec(
        select(Member, Membership)
        .join(Membership, isouter=True)
        .distinct(col(Member.name), col(Member.id))
        .where(col(Member.id).in_(member_ids))
        .order_by(col(Member.name), col(Member.id), col(Membership.exit).desc()),
    ).all()

    return (
        Debiteur(
            member=member,
            membership=membership,
            rows=(member_rows := rows[member.id]),
            amount=sum((row.amount for row in member_rows), ZERO),
        )
        for member, membership in members
    )


def count_months(date_from: datetime.date, date_to: datetime.date) -> float:
    delta = relativedelta(date_to, date_from)
    months_count = delta.months + delta.years * 12.0
    half_month_min_days = 10
    if delta.days >= half_month_min_days:
        months_count += 0.5
    full_month_min_days = 20
    if delta.days >= full_month_min_days:
        months_count += 0.5
    return months_count


def creditor_reference(reference: str) -> str:
    check_digits = 98 - mod_97_10.checksum(f'{reference}RF00')
    return f'RF{check_digits:02}{reference}'


def qr_to_str(qr_bill: QRBill) -> str:
    qr_code = QRCode()
    qr_code.add_data(qr_bill.qr_data())
    qr_lines = [['█' if b else ' ' for b in line] for line in qr_code.get_matrix()]

    middle_y = len(qr_lines) // 2
    middle_x = len(qr_lines[0]) // 2
    qr_lines[middle_y - 3][middle_x - 3 : middle_x + 4] = '       '
    qr_lines[middle_y - 2][middle_x - 3 : middle_x + 4] = ' █████ '
    qr_lines[middle_y - 1][middle_x - 3 : middle_x + 4] = ' ██ ██ '
    qr_lines[middle_y + 0][middle_x - 3 : middle_x + 4] = ' █   █ '
    qr_lines[middle_y + 1][middle_x - 3 : middle_x + 4] = ' ██ ██ '
    qr_lines[middle_y + 2][middle_x - 3 : middle_x + 4] = ' █████ '
    qr_lines[middle_y + 3][middle_x - 3 : middle_x + 4] = '       '

    lines_by_2 = (zip(*pair, strict=False) for pair in zip(*((iter(qr_lines),) * 2), strict=False))

    def get_char(pair: tuple[str, str]) -> str:
        match pair:
            case (' ', ' '):
                return ' '
            case (' ', '█'):
                return '▄'
            case ('█', '█'):
                return '█'
            case ('█', ' '):
                return '▀'
        msg = f'Unexpected pair {pair}'
        raise ValueError(msg)

    qr_str = '\n'.join(''.join(get_char(pair) for pair in line) for line in lines_by_2)
    return qr_str.replace(' ', '\N{NO-BREAK SPACE}')


def qr_to_data_url(qr_bill: QRBill) -> str:
    qr_bill_io = io.StringIO()
    qr_bill.as_svg(qr_bill_io)
    qr_bill_bytes = qr_bill_io.getvalue().encode()
    qr_bill_base64 = b64encode(qr_bill_bytes).decode()
    return f'data:image/svg+xml;base64,{qr_bill_base64}'


def qr_to_pdf(qr_bill: QRBill) -> bytes:
    full_page_io = io.StringIO()
    qr_bill.as_svg(full_page_io, full_page=True)
    full_page_bytes = full_page_io.getvalue().encode()
    rlg = svg2rlg(io.BytesIO(full_page_bytes))
    if rlg is None:
        # svg2rlg returns None instead of raising an exception
        msg = 'svg2rlg failed, see error above this message'
        raise RuntimeError(msg)
    pdf_io = io.BytesIO()
    renderPDF.drawToFile(rlg, pdf_io)
    return pdf_io.getvalue()


if __name__ == '__main__':
    app()
