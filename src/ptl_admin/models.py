import datetime
import enum
from dataclasses import dataclass
from decimal import Decimal
from typing import Any

from sqlalchemy import Identity
from sqlmodel import Enum, Field, Relationship, SQLModel, String, UniqueConstraint
from sqlmodel.main import default_registry

metadata = default_registry.metadata


def varchar_field(*args: Any, max_length: int, **kwargs: Any) -> Any:  # noqa: ANN401
    # https://github.com/tiangolo/sqlmodel/discussions/746
    sa_type: Any = String(length=max_length)
    return Field(*args, max_length=max_length, sa_type=sa_type, **kwargs)


def enum_field(enum: type[enum.Enum], **kwargs: Any) -> Any:  # noqa: ANN401
    sa_type: Any = Enum(enum, native_enum=False)
    return Field(sa_type=sa_type, **kwargs)


class Table(SQLModel):
    id: int = Field(
        default=None,
        primary_key=True,
        sa_column_args=[
            Identity(increment=1, start=1, minvalue=1, maxvalue=2**31 - 1, cache=1, cycle=False),
        ],
    )


class Member(Table, table=True):
    __tablename__ = 'member'

    name: str = varchar_field(max_length=70)
    email: str = varchar_field(max_length=254)
    phonenumber: str | None = varchar_field(max_length=15)
    street: str = varchar_field(max_length=70)
    building: str | None = varchar_field(max_length=16)
    line1: str | None = varchar_field(max_length=70)
    line2: str | None = varchar_field(max_length=70)
    postcode: str = varchar_field(max_length=16)
    city: str = varchar_field(max_length=35)
    country: str = varchar_field(max_length=40)

    memberships: list['Membership'] = Relationship(back_populates='member')
    payments: list['Payment'] = Relationship(back_populates='member')
    bills: list['Bill'] = Relationship(back_populates='member')


class Membership(Table, table=True):
    class Type(str, enum.Enum):
        A = 'actif'
        S = 'support'

    __tablename__ = 'membership'

    member_id: int = Field(foreign_key='member.id')
    type: Type = enum_field(Type, max_length=1)
    entry: datetime.date | None = Field()
    exit: datetime.date | None = Field()

    member: Member = Relationship(back_populates='memberships')


@dataclass(frozen=True)
class ComptaCompteMixin:
    class Type(enum.Enum):
        PRODUITS = 'Produits'
        CHARGES = 'Charges'
        ACTIFS = 'Actifs'
        PASSIFS = 'Passifs'

    type: Type
    label: str


class ComptaCompte(Table, table=True):
    class Type(enum.Enum):
        ACTIFS = 'Actifs'
        PASSIFS = 'Passifs'
        PRODUITS = 'Produits'
        CHARGES = 'Charges'

    __tablename__ = 'compta_compte'

    type: Type = enum_field(Type)
    label: str = varchar_field(max_length=40)

    totaux: list['ComptaTotal'] = Relationship(back_populates='compte')


class CamtDocument(Table, table=True):
    __tablename__ = 'camt_document'

    name: str = varchar_field(max_length=100, unique=True)
    content: bytes = Field(sa_column_kwargs=dict(name='bytes'))

    entries: list['CamtEntry'] = Relationship(back_populates='document')


class CamtEntry(Table, table=True):
    __tablename__ = 'camt_entry'

    document_id: int = Field(foreign_key='camt_document.id')
    reference: str = varchar_field(max_length=35, unique=True)
    xml: str = Field()

    document: CamtDocument = Relationship(back_populates='entries')
    ecritures: list['ComptaEcriture'] = Relationship(back_populates='camt')
    transactions: list['CamtTransaction'] = Relationship(back_populates='entry')


class CamtTransaction(Table, table=True):
    __tablename__ = 'camt_transaction'

    entry_id: int = Field(foreign_key='camt_entry.id')
    reference: str = varchar_field(max_length=35, unique=True)
    xml: str = Field()

    entry: CamtEntry = Relationship(back_populates='transactions')
    payments: list['Payment'] = Relationship(back_populates='camt')


class ComptaTotal(Table, table=True):
    class Type(enum.Enum):
        BUDGET = 'Budget'
        CLOTURE = 'Clôture'

    __tablename__ = 'compta_total'

    year: int = Field()
    compte_id: int = Field(foreign_key='compta_compte.id')
    type: Type = enum_field(Type)
    amount: Decimal = Field(max_digits=8, decimal_places=2)

    compte: ComptaCompte = Relationship(back_populates='totaux')

    __table_args__ = (UniqueConstraint('year', 'type', 'compte_id'),)


class ComptaEcriture(Table, table=True):
    __tablename__ = 'compta_ecriture'

    debit_id: int = Field(foreign_key='compta_compte.id')
    credit_id: int = Field(foreign_key='compta_compte.id')
    date: datetime.date = Field()
    amount: Decimal = Field(max_digits=8, decimal_places=2)
    label: str = varchar_field(max_length=500)
    camt_id: int | None = Field(foreign_key='camt_entry.id')

    camt: CamtEntry | None = Relationship(back_populates='ecritures')
    bill_entries: list['BillEntry'] = Relationship(back_populates='ecriture')


class Bill(Table, table=True):
    __tablename__ = 'bill'
    member_id: int = Field(foreign_key='member.id')
    smartbusiness_id: int | None = Field()
    date: datetime.date = Field()
    isr_ref: str | None = varchar_field(max_length=27)

    member: Member = Relationship(back_populates='bills')
    entries: list['BillEntry'] = Relationship(back_populates='bill')


class BillEntry(Table, table=True):
    __tablename__ = 'bill_entry'

    bill_id: int = Field(foreign_key='bill.id')
    position: int = Field()
    label: str = varchar_field(max_length=60)
    price: Decimal = Field(max_digits=8, decimal_places=2)
    ecriture_id: int | None = Field(foreign_key='compta_ecriture.id')

    bill: Bill = Relationship(back_populates='entries')
    ecriture: ComptaEcriture | None = Relationship(back_populates='bill_entries')

    __table_args__ = (UniqueConstraint('bill_id', 'position'),)


class ComptaAmortissement(Table, table=True):
    __tablename__ = 'compta_amortissement'
    acquisition_id: int = Field(foreign_key='compta_ecriture.id')
    ecriture_id: int = Field(foreign_key='compta_ecriture.id')
    amount: Decimal = Field(max_digits=8, decimal_places=2)

    __table_args__ = (UniqueConstraint('acquisition_id', 'ecriture_id'),)


class Payment(Table, table=True):
    __tablename__ = 'payment'
    # Nullable to keep payments with wrong references
    member_id: int | None = Field(foreign_key='member.id')
    smartbusiness_id: int | None = Field()
    value: Decimal = Field(max_digits=8, decimal_places=2)
    date: datetime.date = Field()
    camt_id: int | None = Field(foreign_key='camt_transaction.id')

    member: Member | None = Relationship(back_populates='payments')
    camt: CamtTransaction | None = Relationship(back_populates='payments')
