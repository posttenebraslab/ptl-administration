# ruff: noqa: T201 (print)

import datetime
from collections.abc import Iterable
from decimal import Decimal
from typing import overload

from airium import Airium  # type: ignore[import-untyped]
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from sqlmodel import Session, col, extract, func, select, union_all

from .database import begin
from .facturation import get_debiteurs
from .models import ComptaCompte, ComptaEcriture, ComptaTotal

ZERO = Decimal('0.00')


app = FastAPI()


@app.get('/bilan.html')
def bilan(year: int) -> HTMLResponse:
    compte_types = (ComptaCompte.Type.ACTIFS, ComptaCompte.Type.PASSIFS)

    with begin() as session:
        amounts_previous = select_totaux(session, year - 1, compte_types, ComptaTotal.Type.CLOTURE)
        amounts_current = select_ecritures(session, year, ComptaCompte.Type.__members__.values())
        comptes = select_comptes(session, amounts_previous, amounts_current)

        # résultat
        amounts_current[22] += sum(
            (amount for compte_id, amount in amounts_current.items() if comptes[compte_id].type not in compte_types),
            ZERO,
        )

        # compute current = previous - current
        for compte_id, amount_previous in amounts_previous.items():
            amount_current = amounts_current.get(compte_id)
            amounts_current[compte_id] = sub(amount_previous, amount_current)

        amounts = get_rows(comptes, amounts_previous, amounts_current)

        session.expunge_all()
        session.rollback()

    a = Airium()

    def rows(compte_type: ComptaCompte.Type, *, negate: bool) -> None:
        sums: Iterable[Decimal] = (ZERO,) * 2

        for compte_id, compte in comptes.items():
            if compte.type != compte_type:
                continue

            row = amounts[compte_id]

            with a.tr():
                a.td(_t=compte.label)
                for amount in row:
                    a.td(class_='right', _t=_t(amount, negate=negate))

            sums = tuple(add(a, b) for a, b in zip(sums, row, strict=True))

        with a.tr(style='font-weight: bold;'):
            a.td(_t=compte_type.value)
            for amount in sums:
                a.td(class_='right', _t=_t(amount, negate=negate))
        with a.tr(style='height: 1em;'):
            a.td()
            for _ in sums:
                a.td()

    a('<!DOCTYPE html>')
    with a.html(lang='fr-CH'):
        with a.head():
            a.meta(charset='utf-8')
            a.meta(name='viewport', content='width=device-width, initial-scale=1')
            a.title(_t='Bilan au 31.12')
            a.style(_t='.right { text-align: right; }')
        with a.body(), a.table(style='border-collapse: collapse'):
            a.caption(_t='Bilan au 31.12')
            with a.colgroup():
                a.col()
                a.col(style='width: 6em;')
                a.col(style='width: 6em; border: solid;')
            with a.thead(style='border-bottom: solid;'), a.tr():
                a.th()
                a.th(class_='right', _t=year - 1)
                a.th(class_='right', _t=year)
            with a.tbody():
                rows(ComptaCompte.Type.ACTIFS, negate=False)
                rows(ComptaCompte.Type.PASSIFS, negate=True)

    return HTMLResponse(str(a))


@app.get('/resultat.html')
def resultat(year: int) -> HTMLResponse:
    compte_types = (ComptaCompte.Type.PRODUITS, ComptaCompte.Type.CHARGES)

    with begin() as session:
        amounts_previous = select_totaux(session, year - 1, compte_types, ComptaTotal.Type.CLOTURE)
        amounts_budget = select_totaux(session, year, compte_types, ComptaTotal.Type.BUDGET)
        amounts_current = select_ecritures(session, year, compte_types)
        amounts_next = select_totaux(session, year + 1, compte_types, ComptaTotal.Type.BUDGET)
        comptes = select_comptes(session, amounts_previous, amounts_budget, amounts_current, amounts_next)

        amounts = get_rows(comptes, amounts_previous, amounts_budget, amounts_current, amounts_next)

        résultats: Iterable[Decimal] = (ZERO,) * 4
        for row in amounts.values():
            résultats = tuple(add(résultat, amount) for résultat, amount in zip(résultats, row, strict=True))

        session.expunge_all()
        session.rollback()

    a = Airium()

    def rows(compte_type: ComptaCompte.Type, *, negate: bool) -> Iterable[Decimal]:
        sums: Iterable[Decimal] = (ZERO,) * 4

        for compte_id, compte in comptes.items():
            if compte.type != compte_type:
                continue

            row = amounts[compte_id]

            with a.tr():
                a.td(_t=compte.label)
                for amount in row:
                    a.td(class_='right', _t=_t(amount, negate=negate))

            sums = tuple(add(a, b) for a, b in zip(sums, row, strict=True))

        with a.tr(style='font-weight: bold;'):
            a.td(_t=compte_type.value)
            for amount in sums:
                a.td(class_='right', _t=_t(amount, negate=negate))
        with a.tr(style='height: 1em;'):
            a.td()
            for _ in sums:
                a.td()

        return sums

    a('<!DOCTYPE html>')
    with a.html(lang='fr-CH'):
        with a.head():
            a.meta(charset='utf-8')
            a.meta(name='viewport', content='width=device-width, initial-scale=1')
            a.title(_t='Pertes et Profits du 01.01 au 31.12')
            a.style(_t='.right { text-align: right; }')
        with a.body(), a.table(style='border-collapse: collapse'):
            a.caption(_t='Pertes et Profits du 01.01 au 31.12')
            with a.colgroup():
                a.col()
                a.col(style='width: 6em;')
                a.col(style='width: 6em;')
                a.col(style='width: 6em; border: solid;')
                a.col(style='width: 6em;')
            with a.thead(style='border-bottom: solid;'):
                with a.tr():
                    a.th()
                    a.th(class_='right')
                    a.th(class_='right', _t='Budget')
                    a.th(class_='right')
                    a.th(class_='right', _t='Budget')
                with a.tr():
                    a.th()
                    a.th(class_='right', _t=year - 1)
                    a.th(class_='right', _t=year)
                    a.th(class_='right', _t=year)
                    a.th(class_='right', _t=year + 1)
            with a.tbody():
                totaux = rows(ComptaCompte.Type.PRODUITS, negate=False)
                rows(ComptaCompte.Type.CHARGES, negate=True)
            with a.tfoot():
                with a.tr(style='font-weight: bold; border: solid;'):
                    a.td(_t='Résultat')
                    for résultat in résultats:
                        a.td(class_='right', _t=_t(résultat, negate=False))
                with a.tr():
                    a.td(_t='Total')
                    for total in totaux:
                        a.td(class_='right', _t=_t(total, negate=False))

    return HTMLResponse(str(a))


@app.get('/debiteurs.html')
def debiteurs(*, verbose: bool = False) -> HTMLResponse:
    with begin() as session:
        shrink_before = datetime.date.min if verbose else datetime.date.max
        debiteurs = get_debiteurs(session, shrink_before)

        session.expunge_all()
        session.rollback()

    a = Airium()
    a('<!DOCTYPE html>')
    with a.html(lang='fr-CH'):
        with a.head():
            a.meta(charset='utf-8')
            a.meta(name='viewport', content='width=device-width, initial-scale=1')
            a.title(_t='Débiteurs')
            a.style(_t='.right { text-align: right; }')
        with a.body(), a.table(style='border-collapse: collapse'):
            a.caption(_t='Débiteurs')
            with a.colgroup():
                a.col()
                a.col()
                a.col()
            for debiteur in debiteurs:
                member = debiteur.member
                membership = debiteur.membership
                rows = debiteur.rows
                if membership is None:
                    membership_str = 'jamais membre'
                elif membership.exit is None:
                    entry_str = 'longtemps' if membership.entry is None else f'le {membership.entry}'
                    membership_str = f'{membership.type.value} depuis {entry_str}'
                else:
                    membership_str = f'inactif depuis le {membership.exit}'
                with a.tr():
                    a.th(colspan=3, _t=f'{member.id} {member.name}')
                with a.tr():
                    a.th(colspan=3, _t=f'({membership_str})')
                for row in rows:
                    with a.tr():
                        a.td(_t=row.date)
                        a.td(_t=row.label)
                        a.td(class_='right', _t=str(row.amount))
                with a.tr(style='font-weight: bold;'):
                    a.td()
                    a.td(_t='solde')
                    a.td(class_='right', _t=str(sum((row.amount for row in rows), ZERO)))
                with a.tr(style='height: 1em;'):
                    a.td()
                    a.td()
                    a.td()

    return HTMLResponse(str(a))


def get_rows(comptes: dict[int, ComptaCompte], *columns: dict[int, Decimal]) -> dict[int, tuple[Decimal | None, ...]]:
    return {compte_id: tuple(column.get(compte_id) for column in columns) for compte_id, compte in comptes.items()}


def select_totaux(
    session: Session,
    year: int,
    compte_types: Iterable[ComptaCompte.Type],
    total_type: ComptaTotal.Type,
) -> dict[int, Decimal]:
    return dict(
        session.exec(
            select(ComptaCompte.id, col(ComptaTotal.amount))
            .join(ComptaCompte)
            .where(
                col(ComptaCompte.type).in_(compte_types),
                ComptaTotal.year == year,
                ComptaTotal.type == total_type,
            ),
        ).all(),
    )


def select_ecritures(session: Session, year: int, compte_types: Iterable[ComptaCompte.Type]) -> dict[int, Decimal]:
    union = union_all(
        select(
            col(ComptaEcriture.date),
            col(ComptaEcriture.credit_id).label('compte_id'),
            col(ComptaEcriture.amount),
        ),
        select(
            col(ComptaEcriture.date),
            col(ComptaEcriture.debit_id),
            -col(ComptaEcriture.amount),
        ),
    ).subquery()
    return dict(
        session.exec(
            select(union.c.compte_id, func.sum(union.c.amount))
            .join(ComptaCompte, col(ComptaCompte.id) == union.c.compte_id)
            .where(col(ComptaCompte.type).in_(compte_types), extract('year', union.c.date) == year)
            .group_by(union.c.compte_id),
        ).all(),
    )


def select_comptes(session: Session, *compte_id_sets: Iterable[int]) -> dict[int, ComptaCompte]:
    compte_ids = (compte_id for compte_ids in compte_id_sets for compte_id in compte_ids)
    return {
        compte.id: compte
        for compte in session.exec(
            select(ComptaCompte).where(col(ComptaCompte.id).in_(compte_ids)).order_by(col(ComptaCompte.id)),
        )
    }


def _t(amount: Decimal | None, *, negate: bool) -> str | None:
    if amount is None:
        return None
    if negate and not amount.is_zero():
        amount = amount.copy_negate()
    return f'{amount:n}'


@overload
def add(a: Decimal, b: Decimal) -> Decimal: ...
@overload
def add(a: None, b: Decimal) -> Decimal: ...
@overload
def add(a: Decimal, b: None) -> Decimal: ...
@overload
def add(a: None, b: None) -> None: ...
def add(a: Decimal | None, b: Decimal | None) -> Decimal | None:
    if a is None:
        if b is None:
            return None
        return b
    if b is None:
        return a
    return a + b


@overload
def sub(a: Decimal, b: Decimal) -> Decimal: ...
@overload
def sub(a: None, b: Decimal) -> Decimal: ...
@overload
def sub(a: Decimal, b: None) -> Decimal: ...
@overload
def sub(a: None, b: None) -> None: ...
def sub(a: Decimal | None, b: Decimal | None) -> Decimal | None:
    if a is None:
        if b is None:
            return None
        return -b
    if b is None:
        return a
    return a - b
