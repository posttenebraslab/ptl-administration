# ruff: noqa: T201 (print)

import os
import re
from collections.abc import Callable
from decimal import Decimal
from io import BytesIO
from pathlib import Path
from typing import Annotated
from xml.etree import ElementTree

import typer
from defusedxml.ElementTree import parse
from paramiko.client import SSHClient
from pydantic import AnyUrl, FilePath, SecretStr, UrlConstraints
from pydantic_settings import BaseSettings
from sqlmodel import Session, col, select

from .database import begin
from .models import Bill, CamtDocument, CamtEntry, CamtTransaction, ComptaEcriture, Member, Payment


def main() -> None:
    app()


class FdsSettings(BaseSettings):
    fds_known_hosts_filename: FilePath = Path(__file__).parent / 'known_hosts'
    fds_uri: Annotated[AnyUrl, UrlConstraints(allowed_schemes=['sftp'], default_port=22)]
    fds_key_filename: FilePath = Path(__file__).parent / 'id_rsa_fds'
    fds_passphrase: SecretStr


app = typer.Typer()


@app.command()
def fds() -> None:
    settings = FdsSettings()

    fds_known_hosts_filename = os.fspath(settings.fds_known_hosts_filename)
    fds_uri = settings.fds_uri
    fds_host = fds_uri.host
    fds_port = fds_uri.port
    fds_user = fds_uri.username
    fds_path = fds_uri.path
    fds_key_filename = os.fspath(settings.fds_key_filename)
    fds_passphrase = settings.fds_passphrase.get_secret_value()
    if fds_host is None or fds_port is None or fds_path is None:
        msg = f'missing host, port or path in {fds_uri}'
        raise ValueError(msg)

    with SSHClient() as client:
        client.load_host_keys(fds_known_hosts_filename)
        client.connect(
            hostname=fds_host,
            port=fds_port,
            username=fds_user,
            key_filename=fds_key_filename,
            passphrase=fds_passphrase,
        )
        sftp = client.open_sftp()
        filenames = sftp.listdir(fds_path)

        def read_bytes(filename: str) -> bytes:
            with sftp.open(f'{fds_path}/{filename}') as stream:
                return stream.read()

        load_files(filenames, read_bytes)


@app.command()
def load(files: tuple[FilePath]) -> None:
    filenames = [file.name for file in files]
    files_map = {file.name: file for file in files}

    def read_bytes(filename: str) -> bytes:
        return files_map[filename].read_bytes()

    load_files(filenames, read_bytes)


@app.command()
def process(files: tuple[FilePath]) -> None:
    with begin() as session:
        for document in session.exec(
            select(CamtDocument).where(col(CamtDocument.name).in_(file.name for file in files)),
        ):
            process_document(session, document)


@app.command()
def process_all() -> None:
    with begin() as session:
        for document in session.exec(select(CamtDocument).order_by(col(CamtDocument.id))):
            process_document(session, document)


def load_files(filenames: list[str], read_bytes: Callable[[str], bytes]) -> None:
    with begin() as session:
        existing = set(session.exec(select(CamtDocument.name).where(col(CamtDocument.name).in_(filenames))))
        for filename in filenames:
            if filename in existing:
                print(filename, 'exists')
                continue

            content = read_bytes(filename)
            print(filename, len(content))

            document = CamtDocument(name=filename, content=content)
            session.add(document)
            process_document(session, document)


# https://www.six-group.com/dam/download/banking-services/interbank-clearing/en/standardization/iso/swiss-recommendations/archives/implementation-guidelines-cm/implementation-guidelines-camt_v1_4.pdf
def process_document(session: Session, document: CamtDocument) -> None:
    compte_ccp_id = 10
    compte_debiteurs_id = 14
    compte_creanciers_id = 20
    namespace_prefix = '{urn:iso:std:iso:20022:tech:xsd:camt.053.001.04}'

    tree = parse(BytesIO(document.content))
    for element in tree.iter():
        element.tag = element.tag.removeprefix(namespace_prefix)

    for entry in tree.findall('BkToCstmrStmt/Stmt/Ntry'):
        entry_reference = entry.findtext('AcctSvcrRef')

        camt_entry = session.exec(select(CamtEntry).where(CamtEntry.reference == entry_reference)).first()
        if camt_entry is None:
            ElementTree.indent(entry, space='\t')
            entry_xml = ElementTree.tostring(entry, encoding='unicode')

            camt_entry = CamtEntry(
                document=document,
                reference=entry_reference,
                xml=entry_xml,
            )
            session.add(camt_entry)

        compta_ecriture = session.exec(select(ComptaEcriture).where(ComptaEcriture.camt == camt_entry)).first()
        if compta_ecriture is None:
            amount = maybe_decimal(entry.findtext('Amt'))
            credit_debit_indicator = entry.findtext('CdtDbtInd')
            booking_date = entry.findtext('BookgDt/Dt')

            match credit_debit_indicator:
                case 'CRDT':
                    debit_id = compte_ccp_id
                    credit_id = compte_debiteurs_id
                    label = 'Encaissement'
                case 'DBIT':
                    debit_id = compte_creanciers_id
                    credit_id = compte_ccp_id
                    label = 'TODO'
                case _:
                    msg = f'unexpected CdtDbtInd {credit_debit_indicator}: {camt_entry.xml}'
                    raise ValueError(msg)

            compta_ecriture = ComptaEcriture(
                debit_id=debit_id,
                credit_id=credit_id,
                date=booking_date,
                amount=amount,
                label=label,
                camt=camt_entry,
            )
            session.add(compta_ecriture)

        value_date = entry.findtext('ValDt/Dt')
        domain_code = entry.findtext('BkTxCd/Domn/Cd')
        family_code = entry.findtext('BkTxCd/Domn/Fmly/Cd')
        sub_family_code = entry.findtext('BkTxCd/Domn/Fmly/SubFmlyCd')

        if ((domain_code, family_code) != ('PMNT', 'RCDT')) or (sub_family_code == 'CHRG'):
            continue

        for transaction in entry.findall('NtryDtls/TxDtls'):
            transaction_reference = transaction.findtext('Refs/AcctSvcrRef')

            camt_transaction = session.exec(
                select(CamtTransaction).where(CamtTransaction.reference == transaction_reference),
            ).first()
            if camt_transaction is None:
                ElementTree.indent(transaction, space='\t')
                transaction_xml = ElementTree.tostring(transaction, encoding='unicode')

                camt_transaction = CamtTransaction(
                    entry=camt_entry,
                    reference=transaction_reference,
                    xml=transaction_xml,
                )
                session.add(camt_transaction)

            payment = session.exec(select(Payment).where(Payment.camt == camt_transaction)).first()
            if payment is None:
                amount = maybe_decimal(transaction.findtext('Amt'))
                reference = transaction.findtext('RmtInf/Strd/CdtrRefInf/Ref')

                member = find_member(session, reference)

                payment = Payment(
                    member=member,
                    value=amount,
                    date=value_date,
                    camt=camt_transaction,
                )
                session.add(payment)

            print(document.name, camt_transaction.reference, payment.member and payment.member.id)


def find_member(session: Session, reference: str | None) -> Member | None:
    if reference is None:
        return None
    if match := re.match('RF[0-9]{2}[0,1]([0-9]+)', reference):
        member_id = int(match.group(1))
        return session.exec(select(Member).where(Member.id == member_id)).first()
    return session.exec(select(Member).join(Bill).where(Bill.isr_ref == reference)).first()


def maybe_decimal(text: str | None) -> Decimal | None:
    if text is None:
        return None
    return Decimal(text)


if __name__ == '__main__':
    main()
