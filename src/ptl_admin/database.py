from collections.abc import Iterator
from contextlib import contextmanager

from pydantic import PostgresDsn, SecretStr
from pydantic_settings import BaseSettings
from sqlmodel import Session, create_engine


class PgSettings(BaseSettings):
    pg_uri: PostgresDsn
    pg_password: SecretStr | None = None

    def get_uri(self) -> str:
        pg_uri = self.pg_uri
        if self.pg_password is not None:
            password = self.pg_password.get_secret_value()
            scheme = pg_uri.scheme
            hosts = pg_uri.hosts()
            for host in hosts:
                host['password'] = password
            path = pg_uri.path
            if path is not None:
                path = path[1:]
            query = pg_uri.query
            pg_uri = PostgresDsn.build(scheme=scheme, hosts=hosts, path=path, query=query)
        return str(pg_uri)


engine = create_engine(PgSettings().get_uri())  # , echo=True)


@contextmanager
def begin() -> Iterator[Session]:
    with Session(engine) as session, session.begin():
        yield session
