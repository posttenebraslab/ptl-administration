from datetime import datetime, timedelta
import io
import xml.etree.ElementTree as ET
from django.core.management.base import BaseCommand
from app.models import FdsFile


class Command(BaseCommand):

    def handle(self, *args, **options):
        delta = timedelta(seconds=1)
        ranges = []
        for file in FdsFile.objects.all():
            document = ET.parse(io.BytesIO(file.bytes))
            for statement in document.findall(xpath('BkToCstmrStmt', 'Stmt')):
                from_date_time = datetime.fromisoformat(statement.findtext(xpath('FrToDt', 'FrDtTm')))
                to_date_time = datetime.fromisoformat(statement.findtext(xpath('FrToDt', 'ToDtTm'))) + delta
                ranges.append((from_date_time, to_date_time))
        ranges.sort()
        prev = ranges.pop(0)[1]
        print('missing FDS files for date ranges:')
        for range in ranges:
            if prev != range[0]:
                print(f'{prev} - {range[0] - delta}')
            prev = range[1]
        print(f'{prev} - ')

def xpath(*names):
    return '/'.join(f'{{urn:iso:std:iso:20022:tech:xsd:camt.053.001.04}}{name}' for name in names)
