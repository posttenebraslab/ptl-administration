# ptl-administration

Admin stuff for the PTL: memberships, billing, accounting, ...

## Setup

- Create a venv

    `python -m venv --upgrade-deps .venv`

- Install Python package dependencies

    `.venv/bin/pip install --constraint requirements-dev.txt --editable '.[dev]'`

## Build

This build is also run by GitLab CI on every push and daily.

- Create container image

    `podman build --tag registry.gitlab.com/posttenebraslab/ptl-administration:latest .`

- Push container image to registry

    `podman push registry.gitlab.com/posttenebraslab/ptl-administration:latest`

## Run locally

- Make the DB accessible on the LAN

    `ssh bebop.lan.posttenebraslab.ch sudo podman run --rm --detach --publish 5432:5432 --network systemd-ptl-admin docker.io/verb/socat TCP-LISTEN:5432,fork TCP-CONNECT:systemd-ptl-admin-postgres:5432`

- Copy `.env.example` to `.env` and fill in the passwords

- Start the web app

    `dotenv run -- .venv/bin/uvicorn --reload ptl_admin.web:app`

- Import files from PostFinance

    `dotenv run -- .venv/bin/python -m ptl_admin.camt fds`

- Use DBeaver for DB edits. There are usefull queries in [compta.sql](compta.sql)

## CI/CD

GitLab CI builds a new host base image on every push: [.gitlab-ci.yml](.gitlab-ci.yml)

## Requirements update

    `.venv/bin/pip-compile --output-file=requirements.txt --strip-extras && .venv/bin/pip-compile --extra=dev --output-file=requirements-dev.txt --strip-extras`
