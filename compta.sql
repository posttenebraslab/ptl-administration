-- title: Plan comptable
select * from compta_compte order by id;

-- title: Member
select * from member order by name, id;

-- title: Memberships
select m.id, m.name, me.* from member m left outer join membership me on m.id = me.member_id order by exit desc, type, name;
select * from membership m order by entry is not null, entry asc, member_id asc;

-- title: Emails
select m.name || ' <' || m.email || '>' from member m join membership me on m.id = me.member_id
where me.exit is null or me.exit >= current_date order by name;

-- title: Ecritures
select ec.id, date, label, debit_id, credit_id, amount, xml
from compta_ecriture ec left outer join camt_entry en on ec.camt_id = en.id
order by date desc, debit_id, credit_id, label;

-- title: Encaissements
select p.id, p.value, date, member_id, xml
from payment p left outer join camt_transaction ct on p.camt_id = ct.id
order by date desc, ct.id;

-- title: Débiteurs
with x(id, date, member_id, account, amount) as (select b.id, b.date, member_id, ce.credit_id, -price from bill_entry be join bill b on be.bill_id = b.id join compta_ecriture ce on be.ecriture_id = ce.id
union all select p.id, date, member_id, 10, value from payment p)
select m.name, m.email, x.*, sum(amount) over (partition by member_id order by name, date, account) from x join member m on x.member_id = m.id order by m.name, date, account;

-- title: Débiteurs
with x(date, member_id, account, amount) as (select b.date, member_id, ce.credit_id, -price from bill_entry be join bill b on be.bill_id = b.id join compta_ecriture ce on be.ecriture_id = ce.id
union all select date, member_id, 10, value from payment p)
select m.id, name, sum(amount) as solde, exit from x join member m on x.member_id = m.id left outer join membership me on m.id = me.member_id group by m.id, me.id having sum(amount) < 0 order by m.name;

-- title: Documents
select id, name, xmlserialize(document convert_from(bytes, 'UTF8')::xml as text indent) from camt_document order by id desc;

-- scratch below
